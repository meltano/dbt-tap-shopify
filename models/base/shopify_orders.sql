with source as (

    select * from {{var('schema')}}.orders

),

renamed as (

    select

        id as order_id,
        customer__id as customer_id,
        checkout_id,

        created_at,
        cancelled_at,
        closed_at,
        processed_at,
        updated_at,

        order_number,

        case
            when source_name = '580111'
                then 'web'
            else source_name
        end as source_name,
        case
            when source_name = 'web' or source_name = '580111'
                then 'Online Store'
            when source_name = 'shopify_draft_order'
                then 'Draft Orders'
            when source_name = 'pos'
                then 'Point of Sale'
            when source_name = 'iphone'
                then 'Shopify POS for iPhone'
            when source_name = 'android'
                then 'Shopify POS for Android'
            when source_name = app_id::text
                then CONCAT('App ', source_name)
            else source_name
        end as source_label,
        currency,

        subtotal_price,
        COALESCE(total_discounts, 0.0) as total_discounts,
        total_line_items_price,
        total_price,
        COALESCE(total_tax, 0.0) as total_tax,
        COALESCE(total_tip_received::numeric, 0.0) as total_tip_received

    from source

)

select * from renamed
