with source as (

    select * from {{var('schema')}}.custom_collections

),

renamed as (

    select

        id as collection_id,

        published_at::timestamp as published_at,
        updated_at::timestamp as updated_at,

        title

    from source

)

select * from renamed
