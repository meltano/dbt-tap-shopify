with source as (

    select * from {{var('schema')}}.order_refunds

),

renamed as (

    select

        id as refund_id,
        order_id,

        created_at,
        processed_at::timestamp as processed_at

    from source

)

select * from renamed
