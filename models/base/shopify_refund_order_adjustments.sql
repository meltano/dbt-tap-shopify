with source as (

    select * from {{var('schema')}}.order_refunds

),

orders as (

    select * from {{var('schema')}}.orders

),

renamed as (

    select

        (adjustments.child->'id')::bigint as refund_order_adjustment_id,
        id as refund_id,
        order_id as order_id,

        adjustments.child->>'kind' as kind,

        (adjustments.child->'amount')::numeric as amount,
        (adjustments.child->'tax_amount')::numeric as tax_amount

    from

        source,
        jsonb_array_elements(source.order_adjustments) adjustments(child)

)

SELECT

    renamed.*,

    orders.currency as currency

FROM renamed
JOIN orders
ON orders.id = renamed.order_id
