with source as (

    select * from {{var('schema')}}.orders

),

renamed as (

    select

        (shipping_lines.child->'id')::bigint as order_shipping_line_id,
        id as order_id,

        currency,

        (shipping_lines.child->'price')::numeric as price,
        (shipping_lines.child->'discounted_price')::numeric as discounted_price

    from

        source,
        jsonb_array_elements(source.shipping_lines) shipping_lines(child)

)

select * from renamed
