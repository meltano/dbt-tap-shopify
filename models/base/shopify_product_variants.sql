with source as (

    select * from {{var('schema')}}.products

),

renamed as (

    select

        (variants.child->'id')::bigint as variant_id,
        id as project_id,

        (variants.child->>'created_at')::timestamp as created_at,
        (variants.child->>'updated_at')::timestamp as updated_at,

        variants.child->>'title' as title,
        variants.child->>'sku' as sku,

        (variants.child->'price')::numeric as price

    from

        source,
        jsonb_array_elements(source.variants) variants(child)

)

select * from renamed
