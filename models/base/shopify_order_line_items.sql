with source as (

    select * from {{var('schema')}}.orders

),

renamed as (

    select

        (line_items.child->'id')::bigint as order_line_item_id,
        id as order_id,
        (line_items.child->>'product_id')::bigint as product_id,
        (line_items.child->>'variant_id')::bigint as variant_id,

        (line_items.child->'quantity')::int as quantity,
        currency,

        (line_items.child->'price')::numeric as price,
        (line_items.child->'total_discount')::numeric as total_discount

    from

        source,
        jsonb_array_elements(source.line_items) line_items(child)

)

select * from renamed
