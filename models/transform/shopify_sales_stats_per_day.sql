with sales as (

     select *
     from {{ref('shopify_sales')}}

),

dates as (

     select *
     from {{ref('shopify_dates')}}

),

aggregated as (

  select

    DATE(MIN(processed_at)) as processed_date,

    currency,

    SUM(order_count) as order_count,
    SUM(gross_sales) as gross_sales,
    SUM(discounts) as discounts,
    SUM(returns) as returns,
    SUM(net_sales) as net_sales,
    SUM(shipping) as shipping,
    SUM(taxes) as taxes,
    SUM(total_sales) as total_sales

  from sales

  group by
    processed_year,
    processed_month,
    processed_day,
    currency

  order by
    processed_year,
    processed_month,
    processed_day,
    currency

)

SELECT 

  dates.date_actual as processed_date,

  currency,

  COALESCE(order_count, 0) as order_count,
  COALESCE(gross_sales, 0) as gross_sales,
  COALESCE(discounts, 0) as discounts,
  COALESCE(returns, 0) as returns,
  COALESCE(net_sales, 0) as net_sales,
  COALESCE(shipping, 0) as shipping,
  COALESCE(taxes, 0) as taxes,
  COALESCE(total_sales, 0) as total_sales

FROM dates
LEFT OUTER JOIN aggregated
ON aggregated.processed_date = dates.date_actual